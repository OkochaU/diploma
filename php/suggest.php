<?php
/* Создадим базу стран и запишем их в массив state (оригинальная база 
содержит названия 73 стран) */
$state[]="Хемингуэй";
$state[]="Пьюзо";
/*.....................*/
$state[]="Булгаков";
$state[]="Шекспир";
//Запишем переданную из JavaScript кода строку в переменную word 
$word=urldecode($_GET["word"]);

/* Если переменная word содержит какие либо символы выполняем дальнейший код
иначе выполнение кода на этом завершается */

if (strlen($word) > 0){
   $suggest="";
   for ($i=0;$i<count($state);$i++){  
    /*Если символы в переменной word совпадают по значению с первыми 
      символами элементов массива state записать их в переменную suggest.
      При этом с помощью substr мы учитываем при сравнении еще и количество переданных 
      символов например если были переданы только два символа их значения будут 
      сравниваться с двумя первыми символами всех элементов массива. Перед сравнением 
      все символы переводятся в нижнюю раскладку. */
   
      if (strtolower($word)==strtolower(substr($state[$i],0,strlen($word)))){
         if ($suggest==""){
            $suggest="<ul id='suggest'onmouseover='this.style.backgroundColor=\"gold\";' style = \"list-style-type: none\"  ><li onmouseover='this.style.backgroundColor= 
            \"gold\";' 
            onmouseout='this.style.backgroundColor=\"white\";' 
            onclick='complete(this.innerHTML)'>".$state[$i]."</li>";
         }
         else {
            $suggest=$suggest."<li' 
            onmouseout='this.style.backgroundColor=\"white\";
            onclick='complete(this.innerHTML)'>".$state[$i]."</li>";
         }
     }
   }

   /* Выводим или найденные совпадения или сообщение об их отсутствии. */

   if ($suggest==""){echo "Совпадения не найдены";}
   else {
      $suggest=$suggest."</ul>"; 
      echo $suggest;
   }
}  
?>
